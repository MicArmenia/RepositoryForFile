﻿using Mic.Lesson.RepositoryForFile.Models;
using Mic.Lesson.RepositoryForFile.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.RepositoryForFile
{
    class Program
    {
        static void Main(string[] args)
        {
            //var studensts = CreateStudents(10).ToArray();
            //var teachers = CreateTeachers(10).ToArray();

            var repository = new TeacherRepository();
            //repository.AddRange(teachers);
            //repository.SaveChanges();

            //IEnumerable<Teacher> enumerable = repository.AsEnumerable().Take(2);
            //List<Teacher> list = enumerable.ToList();

            var item = repository.FiersOrDefault(p => p.Age > 20 && p.Age < 25);
            bool any = repository.Any(p => p.Age == 20);
        }

        static IEnumerable<Student> CreateStudents(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new Student
                {
                    Name = $"A{i + 1}",
                    Surname = $"A{i + 1}yan",
                    Age = (byte)rnd.Next(20, 35),
                    University = (University)rnd.Next(0, 3)
                };
            }
        }

        static IEnumerable<Teacher> CreateTeachers(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new Teacher
                {
                    Name = $"T{i + 1}",
                    Surname = $"T{i + 1}yan",
                    Age = (byte)rnd.Next(20, 35),
                    University = (University)rnd.Next(0, 3)
                };
            }
        }
    }
}
