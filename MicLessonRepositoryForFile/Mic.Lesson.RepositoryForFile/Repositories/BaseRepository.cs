﻿using Mic.Lesson.RepositoryForFile.Attributes;
using Mic.Lesson.RepositoryForFile.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.RepositoryForFile.Repositories
{
    abstract class BaseRepository<T> : IBaseRepository<T>
        where T : class, new()
    {
        protected BaseRepository()
        {
            _fileSet = AsEnumerable().ToList();
        }

        private IList<T> _fileSet;

        public abstract string FileName { get; }

        public void Add(T model)
        {
            _fileSet.Add(model);
        }

        public void AddRange(IEnumerable<T> models)
        {
            foreach (var model in models)
                _fileSet.Add(model);
        }

        public void Insert(int index, T model)
        {
            _fileSet.Insert(index, model);
        }

        public int SaveChanges()
        {
            int count = 0;
            StreamWriter writer = File.CreateText(FileName);
            foreach (var student in _fileSet)
            {
                writer.WriteLine("{");
                WriteModel(writer, student);
                writer.WriteLine("}");

                count++;
            }
            _fileSet.Clear();
            writer.Dispose();

            return count;
        }

        public IEnumerable<T> AsEnumerable()
        {
            if (!File.Exists(FileName))
                File.Create(FileName).Dispose();

            StreamReader reader = File.OpenText(FileName);

            T item = null;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        item = new T();
                        break;
                    case "}":
                        yield return item;
                        break;
                    default:
                        FillModel(line, item);
                        break;
                }
            }

            reader.Dispose();
        }

        //protected abstract void WriteModel(StreamWriter writer, T model);
        //protected abstract  void OnFillModel(string[] data, T model);

        private void FillModel(string line, T model)
        {
            string[] data = line.Split(':');
            if (data.Length < 2)
                return;

            OnFillModel(data, model);
        }

        private void OnFillModel(string[] data, T model)
        {
            string propName = data[0];
            var pi = model.GetType().GetProperty(propName, BindingFlags.Instance | BindingFlags.Public);
            if(pi != null)
            {
                //object value = Convert.ChangeType(data[1], pi.PropertyType);
                if (byte.TryParse(data[1], out byte value))
                    pi.SetValue(model, value);
                else
                    pi.SetValue(model, data[1]);

                //pi.SetValue(model, value);
            }
        }

        public bool IsNullable(Type type)
        {
            return Nullable.GetUnderlyingType(type) != null;
        }

        private void WriteModel(StreamWriter writer, T model)
        {
            //PropertyInfo[]
            var pis = model.GetType().GetProperties();
            foreach (var pi in pis)
            {
                //var ignore = pi.GetCustomAttribute<IgnoreAttribute>();
                //if (IsIgnored(pi.Name))
                if (pi.IsIgnored())
                    continue;

                object value = pi.GetValue(model);
                if (pi.PropertyType.IsEnum)
                    writer.WriteLine($"{pi.Name}:{Convert.ToInt32(value)}");
                else
                    writer.WriteLine($"{pi.Name}:{value}");
            }
        }

        public T FiersOrDefault(Func<T, bool> predicate) =>
            _fileSet.FirstOrDefault(predicate);

        public bool Any(Func<T, bool> predicate) =>
            _fileSet.Any(predicate);

        //protected abstract bool IsIgnored(string propertyName);
    }
}