﻿using Mic.Lesson.RepositoryForFile.Models;
using System.Collections.Generic;
using System.IO;

namespace Mic.Lesson.RepositoryForFile.Repositories
{
    class TeacherRepository : BaseRepository<Teacher>
    {
        public override string FileName => "Teachers.txt";

        //protected override bool IsIgnored(string propertyName)
        //{
        //    if (propertyName == nameof(Teacher.Fullname))
        //        return true;
        //    return false;
        //}

        //protected override void OnFillModel(string[] data, Teacher model)
        //{
        //    switch (data[0])
        //    {
        //        case nameof(Teacher.Name):
        //            model.Name = data[1];
        //            break;

        //        case nameof(Teacher.Surname):
        //            model.Surname = data[1];
        //            break;

        //        case nameof(Teacher.Age):
        //            if (byte.TryParse(data[1], out byte age))
        //                model.Age = age;
        //            break;

        //        case nameof(Teacher.University):
        //            if (byte.TryParse(data[1], out byte university))
        //                model.University = (University)university;
        //            break;
        //    }
        //}

        //protected override void WriteModel(StreamWriter writer, Teacher model)
        //{
        //    writer.WriteLine($"{nameof(model.Name)}:{model.Name}");
        //    writer.WriteLine($"{nameof(model.Surname)}:{model.Surname}");
        //    writer.WriteLine($"{nameof(model.Age)}:{model.Age}");
        //    writer.WriteLine($"{nameof(model.University)}:{(byte)model.University}");
        //}
    }
}
