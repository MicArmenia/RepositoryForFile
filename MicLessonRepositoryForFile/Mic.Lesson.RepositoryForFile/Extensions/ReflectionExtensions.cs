﻿using Mic.Lesson.RepositoryForFile.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.RepositoryForFile.Extensions
{
    public static class ReflectionExtensions
    {
        public static bool IsIgnored(this PropertyInfo member)
        {
            return member.GetCustomAttribute<IgnoreAttribute>() != null;
        }
    }
}