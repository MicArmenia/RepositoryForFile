﻿using Mic.Lesson.RepositoryForFile.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.RepositoryForFile.Models
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
        public University University { get; set; }

        [Ignore]
        public string Fullname => $"{Name} {Surname}";

        public override string ToString() => Fullname;
    }
}
