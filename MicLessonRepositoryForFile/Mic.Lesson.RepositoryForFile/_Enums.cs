﻿namespace Mic.Lesson
{
    public enum University : byte
    {
        Politexnik,
        PetHamalsaran,
        Mankavarjakan
    }
}